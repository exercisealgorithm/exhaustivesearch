/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.exhaustivesearch;

/**
 *
 * @author hanam
 */
public class TestEXS {
     public static void main(String[] args) {
        int[] a = {1, 3, 5, 7, 10, 4, 8};
        showInput(a);
        EXS exs = new EXS(a);
        exs.process();
//        exs.getDisjointArray1();
//        System.out.println("------------------------");
//        exs.getDisjointArray2();
        exs.sum();

    }

    private static void showInput(int[] a) {
        System.out.print("Input is: ");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println("");
        System.out.println("");
    }

}
